<?php

require 'config/credentials.php';
require 'vendor/autoload.php';

Use \Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
  'driver' => 'mysql',
  'host' => $db_host,
  'database' => $db_name,
  'username' => $db_user,
  'password' => $db_password,
  'charset'  => 'utf8',
  'collation' => 'utf8_unicode_ci', 
  "prefix" => '',
]);

$capsule->bootEloquent();

