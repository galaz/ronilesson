<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());

//------------------hw1

$app->get('/customers/{number}', function($request, $response,$args){
    $json = '{"1":"john", "2":"jack"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['number'], $array)){
        echo $array[$args['number']];
    }
    else{
        echo "This user not found";
    }

});

//--------------------

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']);
    $message->delete();
    
   if($message->exsits){
       return $response->withStatus(400);
   }
   else{
       return $response->withStatus(200);
   }
});

$app->put('/messages/{message_id}', function($request, $response, $args){
    $message = $request->getParsedBodyParam('message',''); // מחרוזת מה POST 
    $_message = Message::find($args['message_id']);//לקיחה מהמסד את האובייקט הרלוונטי לפי המשיכה מה URL
    die("message id is: ".$_message->id);

    $_message->body = $message;// העדכון

   if($_message->save()){
        $payload = ['message_id'=>$_message->id, "result"=>"the message has been update succsesfully "]; // יצירת מערך כדי להחזיר עם JSON 
        return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});

$app->post('/messages/bulk', function($request, $response,$args){
   $payload = $request->getParsedBody();// הופך את ה JSON למערך
    Message::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});
//---------------------user - hw2

$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
   $email = $request->getParsedBodyParam('email','');
   $_user = new User();
   $_user->email = $email;
   $_user->save();
   if($_user->id){
       $payload = ['email:'=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

//user bulk HW---------------------------------------------------------
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    User::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});

$app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    User::trash($payload);
    return $response->withStatus(200)->withJson($payload);

});

$app->run();