<?php
namespace Chatter\Middleware;

class Logging{
    public function __invoke($request, $response,$next){
        // before route
        error_log($request->getMethod()."--".$request->getUri()."\r\n",3,"log.text");
        $response = $next($request,$response);
        return $response;
        //after route
        }
}